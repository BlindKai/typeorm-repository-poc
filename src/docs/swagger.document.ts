import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

export function useSwagger(app: INestApplication) {
  const config = new DocumentBuilder()
    .setTitle('Music API')
    .setDescription('This docs show PoC for TypeORM 0.3.x custom repositories')
    .setVersion('0.0.0a')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);
}
