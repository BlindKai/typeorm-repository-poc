import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { ApiOkPaginatedResponse } from '../../utils/pagination/pagination.decorator';
import { PaginationDto } from '../../utils/pagination/pagination.dto';
import { AuthorsService } from './authors.service';
import { CreateAuthorDto } from './dto/create-author.dto';
import { UpdateAuthorDto } from './dto/update-author.dto';
import { Author } from './entities/author.entity';

@Controller('authors')
export class AuthorsController {
  constructor(private readonly authorsService: AuthorsService) {}

  @Post()
  create(@Body() createAuthorDto: CreateAuthorDto) {
    return this.authorsService.create(createAuthorDto);
  }

  @ApiOkPaginatedResponse(Author)
  @Get()
  findAll(@Query() { limit, offset }: PaginationDto) {
    return this.authorsService.findAll(limit, offset);
  }

  @Get(':authorId')
  findOne(@Param('authorId') id: number) {
    return this.authorsService.findOne(id);
  }

  @Patch(':authorId')
  update(@Param('authorId') id: number, @Body() updateAuthorDto: UpdateAuthorDto) {
    return this.authorsService.update(id, updateAuthorDto);
  }

  @Delete(':authorId')
  remove(@Param('authorId') id: number) {
    return this.authorsService.remove(id);
  }
}
