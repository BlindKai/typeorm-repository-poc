export class GetAuthorsListDto {
  authorId: number;
  firstName: string;
  lastName: string;
  born: Date;
}
