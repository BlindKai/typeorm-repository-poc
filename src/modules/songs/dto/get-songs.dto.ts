import { IsOptional, IsString, MinLength } from 'class-validator';
import { PaginationDto } from '../../../utils/pagination/pagination.dto';

export class GetSongsListDto extends PaginationDto {
  @MinLength(1)
  @IsString()
  @IsOptional()
  search?: string;
}

export class GetSongsListResponseDto {
  songId: number;
  name: string;
  year: number;
  genre: string;
  authorId: number;
}
