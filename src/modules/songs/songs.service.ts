import { Injectable } from '@nestjs/common';
import { FindManyOptions, ILike } from 'typeorm';
import { Paginated } from '../../utils/pagination/pagination.dto';
import { CreateSongDto } from './dto/create-song.dto';
import { UpdateSongDto } from './dto/update-song.dto';
import { Song } from './entities/song.entity';
import { SongsRepository } from './songs.repository';

@Injectable()
export class SongsService {
  constructor(private readonly songsRepository: SongsRepository) {}

  create(createSongDto: CreateSongDto): Promise<Song> {
    return this.songsRepository.save(createSongDto);
  }

  async findAll(take: number, skip: number, search?: string): Promise<Paginated<Song>> {
    const options: FindManyOptions<Song> = {
      take,
      skip,
    };

    if (search) options.where = { name: ILike(`%${search}%`) };

    const [items, count] = await this.songsRepository.findAndCount(options);

    return { items, count };
  }

  async findOne(songId: number): Promise<Song> {
    return this.songsRepository.findOne({ where: { songId } });
  }

  async update(songId: number, updateSongDto: UpdateSongDto) {
    return this.songsRepository.update({ songId }, updateSongDto);
  }

  async remove(songId: number) {
    return this.songsRepository.delete({ songId });
  }
}
