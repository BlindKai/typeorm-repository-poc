import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Song } from './entities/song.entity';

@Injectable()
export class SongsRepository extends Repository<Song> {
  constructor(@InjectRepository(Song) repository: Repository<Song>) {
    super(repository.target, repository.manager, repository.queryRunner);
  }
}
