import { Author } from '../../authors/entities/author.entity';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'songs' })
export class Song {
  @PrimaryGeneratedColumn('increment', { name: 'song_id' })
  songId: number;

  @Column({ type: 'varchar', length: 128, unique: true })
  name: string;

  @Column({ type: 'smallint' })
  year: number;

  @Column({ type: 'varchar', length: 64 })
  genre: string;

  @Column({ type: 'integer', name: 'author_id' })
  authorId: number;

  @ManyToOne(() => Author, (author) => author.authorId)
  @JoinColumn({ name: 'author_id', referencedColumnName: 'authorId', foreignKeyConstraintName: 'fk_song_author' })
  author: Author;
}
