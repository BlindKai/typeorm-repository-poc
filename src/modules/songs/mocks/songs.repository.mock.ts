import { Song } from '../entities/song.entity';
import { SongsRepository } from '../songs.repository';
import { mockSongs } from './songs.mock';

export function filterByNameHelper<T>(array: T[], property: keyof T, searchBy: string) {
  return array.filter((item) => String(item[property]).includes(searchBy));
}

export const mockSongsRepository: jest.Mocked<Partial<SongsRepository>> = {
  findAndCount: jest.fn(async (options) => {
    let songs = mockSongs;

    if (options.where?.['name']) {
      const searchBy = options.where['name'].value.replaceAll('%', '');
      songs = filterByNameHelper(mockSongs, 'name', searchBy);
    }

    const paginated = songs.slice(options.skip || 0, options.skip + options.take || songs.length);
    return [paginated, songs.length] as [Song[], number];
  }),
};
