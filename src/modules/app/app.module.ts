import { Module } from '@nestjs/common';
import { CommonModule } from '../common/common.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SongsModule } from '../songs/songs.module';
import { AuthorsModule } from '../authors/authors.module';

@Module({
  imports: [CommonModule, SongsModule, AuthorsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
