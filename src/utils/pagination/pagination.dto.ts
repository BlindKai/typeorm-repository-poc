import { ApiProperty, ApiHideProperty } from '@nestjs/swagger';
import { Transform, Expose } from 'class-transformer';
import { IsInt, IsPositive, Max, IsDefined } from 'class-validator';

export class PaginationDto {
  @IsInt()
  @IsPositive()
  @Transform(({ value }) => Number(value))
  @ApiProperty({ example: 1, minimum: 1, type: 'integer' })
  page: number;

  @IsInt()
  @IsPositive()
  @Max(1000)
  @Transform(({ value }) => Number(value))
  @ApiProperty({ example: 10, minimum: 1, maximum: 1000, type: 'integer' })
  pageSize: number;

  @ApiHideProperty()
  @Expose()
  @IsDefined()
  @Transform(({ obj }) => obj.pageSize * (obj.page - 1))
  offset: number;

  @ApiHideProperty()
  @Expose()
  @IsDefined()
  @Transform(({ obj }) => Number(obj.pageSize))
  limit: number;
}

export class Paginated<T> {
  items: T[];
  count: number;
}
