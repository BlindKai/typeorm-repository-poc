export type TestCase = {
  description: string;
  querystring?: string;
  requestBody?: Record<string, any>;
  responseBody?: Record<string, any>;
  statusCode: number;
};
