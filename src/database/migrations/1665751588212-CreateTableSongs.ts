import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateTableSongs1665751588212 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    return queryRunner.query(`CREATE TABLE songs (
          song_id SERIAL PRIMARY KEY,
          name VARCHAR(128) UNIQUE,
          year SMALLINT,
          genre VARCHAR(64),
          author_id INT,
          CONSTRAINT fk_song_author FOREIGN KEY (author_id) REFERENCES authors(author_id)
        );`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    return queryRunner.query('DROP TABLE songs;');
  }
}
